set spell
" vimtex
let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
let g:vimtex_quickfix_mode=0
nnoremap <Leader>cv <Plug>(vimtex-view)
" tex-conceal.vim
set conceallevel=1
let g:tex_conceal='abdmg'
hi Conceal ctermbg=none
let g:vimtex_compiler_latexmk = {
    \ 'build_dir' : 'out_dir',
    \ 'callback' : 1,
    \ 'continuous' : 1,
    \ 'executable' : 'latexmk',
    \ 'hooks' : [],
    \ 'options' : [
    \   '-verbose',
    \   '-file-line-error',
    \   '-synctex=1',
    \   '-interaction=nonstopmode',
    \ ],
    \}


inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u
