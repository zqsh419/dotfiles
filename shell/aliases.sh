#############################RC FILE############################################
if [[ $SHELL == '/bin/bash' ]]; then
    rc_file='~/.bashrc'
else
    rc_file='~/.zshrc'
fi

alias rebash="source $rc_file"
alias eb='code ~/dotfiles'
export SHPATH=$HOME/dotfiles/shell
alias ealias='vim $SHPATH'
alias essh='vim ~/.ssh/config'
alias etask='vim ~/dotfiles/config/asynctask/tasks.ini'
function ddot() {
    TInfo "download dotfiles and merge to current dot branch"
    git -C $HOME/dotfiles pull origin dev
    git -C $HOME/dotfiles merge dev
}
function udot() {
    TInfo "merge local to dev in dot and push dev"
    # use git branch --show-current      >= git 2.22
    cur_branch=$(git -C $HOME/dotfiles rev-parse --abbrev-ref HEAD)
    git -C $HOME/dotfiles checkout dev
    git -C $HOME/dotfiles merge $cur_branch
    git -C $HOME/dotfiles push origin
    git -C $HOME/dotfiles checkout $cur_branch
}

function dall() {
    ddot
    dnv
}
function uall() {
    udot
    unv
}
#############################END###############################################

if [ -n "$ZSH_VERSION" ]; then
    ## bindkey
    # practice https://jdhao.github.io/2019/06/13/zsh_bind_keys/
    # doc of bindkey https://zsh.sourceforge.io/Doc/Release/Zsh-Line-Editor.html#:~:text=18.3%20Zle-,Builtins,-The%20ZLE%20module
    bindkey -s '^@' 'asynctask -f\n'
    bindkey -s '^P' '!!\n'
fi

#############################SYSTEM############################################
alias sleep_down="systemctl suspend"
alias pid="ps -Flww -p"
alias k9="kill -9"
alias p8="ping 8.8.8.8"
alias task='asynctask -f'
function ssport() {
    ssh -L "$1":localhost:"$1" "$2"
}
alias remove='/bin/rm -irv'
alias frm='/bin/rm -rf'
alias rm='trash-put'
alias copy='/bin/cp -r'
alias cp='rsync -rzvhP' # verbose human readable progress zipped
alias C='xclip -sel clipboard'
alias CF='xclip-copyfile'
alias PF='xclip-pastefile'

rcp() {
    arg1=${1}
    arg2=${2}
    arr1=(${arg1//:/ })
    arr2=(${arg2//:/ })
    # bash test.sh g:/tmp/abc.txt mq:/tmp/
    # ssh -R localhost:50000:mq:22 g 'rsync -e "ssh -p 50000" -vuar /var/www localhost:/var/www'
    # echo "localhost:50000:${arr2[0]}:22 ${arr1[0]}" "rsync -e 'ssh -p 50000' -vuar ${arr1[1]} localhost:${arr2[1]}"
    ssh -R localhost:50000:${arr2[0]}:22 ${arr1[0]} rsync -e 'ssh -p 50000' -vuar ${arr1[1]} localhost:${arr2[1]}
}

# rsync --filter=': -.gitignore'

fkill() {
    local pid
    if [ "$UID" != "0" ]; then
        pid=$(ps -f -u $UID | sed 1d | fzf -m | awk '{print $2}')
    else
        pid=$(ps -ef | sed 1d | fzf -m | awk '{print $2}')
    fi

    if [ "x$pid" != "x" ]; then
        echo $pid | xargs kill -${1:-9}
    fi
}

# fh - repeat history
fh() {
    print -z $( ([ -n "$ZSH_NAME" ] && fc -l 1 || history) | fzf +s --tac | sed -r 's/ *[0-9]*\*? *//' | sed -r 's/\\/\\\\/g')
}
