# Python startup file
export PYTHONSTARTUP=$HOME/.pythonrc
# Python code env
export HYDRA_FULL_ERROR=1
export WANDB_START_METHOD=thread
# export TFDS_DATA_DIR=$HOME/dpdata/tensorflow_datasets
# Set breakpoint() in Python to call pudb
# export PYTHONBREAKPOINT="pudb.set_trace"
# export JAM_PDB=PUDB

# Vagrant
VAGRANT_DISABLE_VBOXSYMLINKCREATE=1

# latex helpers path
export TEXINPUTS=.:$HOME/dotfiles/latex//:

# HF
if [ -d "$HOME/dpdata" ]
then
    export TRANSFORMERS_CACHE=$HOME/dpdata/huggingface/transformers/
    export HF_HOME=$HOME/dpdata/huggingface/
fi