
# open
if [ "$(uname)" = "Linux" ]; then
  if type xdg-open &>/dev/null; then
    alias open='xdg-open $@ 2>/dev/null'
  else
    alias open='gnome-open $@ 2>/dev/null'
  fi
fi
alias o='open'
alias o.='open .'
alias gcal='open https://www.google.com/calendar/render#g >/dev/null 2>&1'
alias gmail='open https://mail.google.com/mail/u/0/ >/dev/null 2>&1'
alias fl='nautilus'

export HAS_SUDO=false