alias ca="conda activate"
alias cda="conda deactivate"

export PYTHONPATH="$HOME/jam:$PYTHONPATH"

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
conda_path="$HOME/anaconda3/bin/conda"
__conda_setup="$($conda_path 'shell.bash' 'hook' 2>/dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "$HOME/anaconda3/etc/profile.d/conda.sh" ]; then
        . "$HOME/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="$HOME/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

function debug() {
    export DEBUG="PYTHON DEBUG"
}
function release() {
    unset DEBUG
}
function conda_install() {
    # FROM https://gist.github.com/kauffmanes/5e74916617f9993bc3479f401dfec7da
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh
    bash ~/miniconda.sh -b -p ~/anaconda3
    rm ~/miniconda.sh
}

function unit() {
    local file
    file=$(fzf +m -q "$1") && python -m unittest "$file"
}
function pkl() {
    local file
    file=$(fzf +m -q "$1") && jam-inspect-file "$file"
}

#############################jupyter###########################################

alias jlremote="jupyter lab --no-browser --port=8888"

alias jlabLocal="ssh -Y -N -L localhost:8888:localhost:8888"

#-------------------------I do not know it can work well--------------
function jllocal() {
    cmd="ssh -Y -N -L localhost:8888:localhost:8888 qzhang419@gpu"
    running_cmds=$(ps aux | grep -v grep | grep "$cmd")
    if [[ "$1" == 'kill' ]]; then
        for pid in $(echo $running_cmds | awk '{print $2}'); do
            echo "kill pid $pid"
            kill -9 $pid
        done
    else
        if [ !-z $n_running_cmds ]; then
            echo "jllocal command is still running, Kill with 'jllocla kill' next time"
        else
            echo "Running cmmand '$cmd'"
            eval "$cmmd"
        fi
        url=$(ssh qhang419@gpu \
            \
            '~' | # 'REMOTE/PATH/TO/jupyter notebook list' \
            grep http | awk '{print $1}')
        echo "URL that will open in your browser:"
        echo "$url"
        open "$url"
    fi
}
#############################END###############################################

#### fzf ####
_fzf_complete_jam-run() {
    _fzf_complete  --prompt="jam-run> " -- "$@" < <(
    tac .jam_cmds
)
}
