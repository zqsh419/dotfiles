#!/usr/bin/env python3

import argparse
import json
from subprocess import PIPE, Popen
from datetime import datetime, timezone
from collections import defaultdict

def run_simple_command(cmd: str):
    cmd = cmd.split(" ")
    with Popen(cmd, stdout=PIPE, stderr=PIPE) as prc:
        stdout, stderr = prc.communicate()
        stdout = stdout.decode("utf-8")
        stderr = stderr.decode("utf-8")
        prc.terminate()
    return stdout, stderr

parser = argparse.ArgumentParser()
parser.add_argument('-A', '--kill-all', default=False, action='store_true',help='kill all jobs')
parser.add_argument('--key', default='qsh', type=str)
parser.add_argument('-D', '--debug', default=True, action='store_false',help='debug only print')
parser.add_argument('-T', '--team', default=False, action='store_true',help='defualt use lpr-imagine')
args = parser.parse_args()

team = 'deep-imagination' if args.team else 'lpr-imagine'

stdout, stderr = run_simple_command(f'ngc batch list --format_type json --team {team}')
print_info = defaultdict(list)
if stderr:
    exit(0)
else:
    list_job = json.loads(stdout)
    for job in list_job:
        status = job['jobStatus']['status']
        name = job['jobDefinition']['name']
        job_id = job['id']
        if status in ['STARTING', 'RUNNING', 'QUEUED']:
            if args.debug:
                if status == 'RUNNING':
                    s_t = datetime.strptime(job['jobStatus']['startedAt'], '%Y-%m-%dT%H:%M:%S.000Z')
                    cur_t = datetime.now(timezone.utc).replace(tzinfo=None)
                    td = (cur_t - s_t)
                    print_info[status].append(f"{job_id:<15} {name:<35} {status:<10} {td.days}D {td.seconds//3600}H {(td.seconds//60)%60}M")
                elif status == 'QUEUED':
                    s_t = datetime.strptime(job['jobStatus']['queuedAt'], '%Y-%m-%dT%H:%M:%S.000Z')
                    cur_t = datetime.now(timezone.utc).replace(tzinfo=None)
                    td = (cur_t - s_t)
                    print_info[status].append(f"{job_id:<15} {name:<35} {status:<10} {td.days}D {td.seconds//3600}H {(td.seconds//60)%60}M")
                else:
                    print_info[status].append(f"{job_id:<15} {name:<35} {status:<10}")
            else:
                if args.kill_all:
                    run_simple_command(f"ngc batch kill {job_id}")
                elif args.key in name:
                    run_simple_command(f"ngc batch kill {job_id}")

for k,v in print_info.items():
    print("#"*10 + f" {team}  {k}  Total {len(v):<10}" + "#"*10)
    for item in v:
        print(item)