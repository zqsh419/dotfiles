# Branch Organize

## TODO

- [x] Fix trash-cli issue.
- [x] consider change the default python3 interpreter
- [x] `Python2.7` should be addressed seperately! 
- [x] create config folder, check if the `.local/bin` exist, do not remove same as `.config`

## Known Issues or Strange features

-
```shell
echo --test (0.4,0.3)
# this is because the globbing magic by zsh use instead
echo --test '(0.4,0.3)' # useful in python
```

## Add plugins

### Example
```shell
git submodule add https://github.com/zsh-users/zsh-autosuggestions.git zsh/plugins/zsh-autosuggestions
```

## ToLearn
- [fasd](https://github.com/clvv/fasd) Prezto has bundled fasd. `j` changes the current working directory. Type `,`, `f,`, `d,` in front of a comma-separated query or type `,,`,  `,,f`,`,,d` at the end of a comma-separated query then hit <kbd>tab</kbd>.
- [fd](https://github.com/sharkdp/fd) `Note` I already place the `bin` and `share` in `.local`. If need to install, run `apt install` or install from `.deb`. 
- rsync
  - `rsync-copy` copies files and directories from *source* to *destination*.
  - `rsync-move` moves files and directories from *source* to *destination*.
  - `rsync-update` updates files and directories on *destination*.
  - `rsync-synchronize` synchronizes files and directories between *source* and
    *destination*.

## MISC
- [Install without sudo](https://askubuntu.com/a/350/740124)
- [make install specify path](https://stackoverflow.com/questions/3239343/make-install-but-not-to-default-directories/3239373)

# Thanks
Now the structure is borrow from  [missing](https://github.com/anishathalye/dotfiles), directly run install can deploy directly. 

## Version

### [**0.0.1**](https://gitlab.com/zqsh419/dotfiles/-/issues/1)

1. [delta](https://github.com/dandavison/delta) *git diff*, *git show*, *git log -p*, *git stash show -p
*, *git reflog -p*, *git add -p*
2. [forgit](https://github.com/wfxr/forgit) *ga*, *glo*, *gi*, *gd*, *grh*, *gcf*, *gcp*, *grb*
3. Basic Setup. Terminal completion. fzf, j, tmux, vim

### [**0.0.2**]()

1. Fix vim project folder bug. Really annoying previous
2. [syntastic](https://github.com/vim-syntastic/syntastic#settings) is a powerful tool.
3. Gives more error checking and formatter for python mode.

### [**0.0.3**](https://gitlab.com/zqsh419/dotfiles/-/issues/9)

1. Adding [quick cheat tools](https://gitlab.com/zqsh419/dotfiles/-/issues/7)
2. Adding more alias and organize modules. Eg. quick jump in terminal see [cd.sh](shell/modules/cd.sh)
3. Useful alias `o`, `udot`,`mdot`,`here`,`$there`.