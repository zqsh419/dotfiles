c.TerminalInteractiveShell.editing_mode = "vi"
c.InteractiveShellApp.extensions = ["autoreload"]
c.InteractiveShellApp.exec_lines = [
    "%autoreload 2",
    "%config Completer.use_jedi = False",
    "import numpy as np",
    "import matplotlib.pyplot as plt",
    "import matplotlib",
    "%matplotlib inline",
    "try:\n\timport torch as th\n\timport torch\nexcept ImportError:\n\tpass",
    "try:\n\timport jammy.io as jio\nexcept ImportError:\n\tpass",
    "try:\n\tfrom jammy.utils.printing import stprint\nexcept ImportError:\n\tpass",
    "try:\n\timport jamviz.jupyter as jjpt\nexcept ImportError:\n\tpass",
    "try:\n\timport jamviz as jviz\nexcept ImportError:\n\tpass",
    "try:\n\tfrom tqdm.auto import tqdm\nexcept ImportError:\n\tpass",
    "import os, pathlib, sys",
    "if not 'ipynb_path' in globals():\n\tipynb_path = os.getcwd()\n\tipynb_rpath = pathlib.Path(ipynb_path).parent.resolve()\n\tsys.path.insert(0, str(ipynb_rpath))",
    "def parent_path(suffix):\n\treturn os.path.join(ipynb_rpath, suffix)",
]
