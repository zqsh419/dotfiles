# TODO: write a python script is better / easy exec debug shell!!!
alias ngc_debug_cpu='ngc batch run -f ~/dotfiles/shell/modules/_ngc/cpu_debug.json --priority HIGH --order 1 --preempt RUNONCE'

alias ngc_debug_16='ngc batch run -f ~/dotfiles/shell/modules/_ngc/16_debug.json --priority HIGH --order 1 --preempt RUNONCE'
alias ngc_debug_16x4='sed "s/16g.1/16g.4/g" ~/dotfiles/shell/modules/_ngc/16_debug.json > /tmp/_debug.json; ngc batch run -f /tmp/_debug.json --priority HIGH --order 1 --preempt RUNONCE'
alias ngc_debug_16x8='sed "s/16g.1/16g.8/g" ~/dotfiles/shell/modules/_ngc/16_debug.json > /tmp/_debug.json; ngc batch run -f /tmp/_debug.json --priority HIGH --order 1 --preempt RUNONCE'

alias ngc_debug_32='sed "s/16g/32g/g" ~/dotfiles/shell/modules/_ngc/16_debug.json > /tmp/_debug.json; ngc batch run -f /tmp/_debug.json --priority HIGH --order 1 --preempt RUNONCE'
alias ngc_debug_32x4='sed "s/16g.1/32g.4/g" ~/dotfiles/shell/modules/_ngc/16_debug.json > /tmp/_debug.json; ngc batch run -f /tmp/_debug.json --priority HIGH --order 1 --preempt RUNONCE'
alias ngc_debug_32x8='sed "s/16g.1/32g.8/g" ~/dotfiles/shell/modules/_ngc/16_debug.json > /tmp/_debug.json; ngc batch run -f /tmp/_debug.json --priority HIGH --order 1 --preempt RUNONCE'