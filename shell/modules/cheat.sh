alias cht="cheat"
alias echt="vim ~/.dotfiles/local/share/cheat/cheatsheets/personal"
alias qd='googler -n 3 define'

# cheat
export CHEAT_CONFIG_PATH="$HOME/dotfiles/local/share/cheat/conf.yml"
#autoload ~/dotfiles/local/share/cheat/_cheat.zsh
export CHEAT_USE_FZF=true


# navi
alias nv="navi"
alias jnv="cd \"$(navi info cheats-path)/navi\""
function unv() {
    TInfo "save and push navi snippets"
    git -C "$(navi info cheats-path)/navi" add -A
    git -C "$(navi info cheats-path)/navi" commit -m "save"
    git -C "$(navi info cheats-path)/navi" push origin
}

function dnv() {
    TInfo "download navi snippets"
    if [ ! -d  "$(navi info cheats-path)/navi" ]; then
        git clone git@gitlab.com:qsh.zh/navi.git "$(navi info cheats-path)/navi"
    fi
    git -C "$(navi info cheats-path)/navi" pull
}

alias envi="vim $(navi info cheats-path)/navi"

if [ -n "$ZSH_VERSION" ]; then
    eval "$(navi widget zsh)"
elif [ -n "$BASH_VERSION" ]; then
    eval "$(navi widget bash)"
else
    echo "Not support shell"
fi

function cnv() {
    replacement="$(navi --print < /dev/tty)"
    echo $replacement
}