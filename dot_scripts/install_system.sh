set -eu

apt-get update

 
# terminal enhance
apt-get install -y --fix-missing --no-install-recommends\
    htop ctop graphviz openssh-client autossh powerline software-properties-common

add-apt-repository -y ppa:ubuntu-toolchain-r/test
# system
apt-get install -y --fix-missing --no-install-recommends\
    build-essential ca-certificates sudo git vim zsh curl tmux wget iputils-ping mlocate xclip xvfb tree less software-properties-common ack ctags silversearcher-ag cmake g++-9 gcc-9 python3-dev 


# powerline-gitstatus 

apt-get install -y --fix-missing --no-install-recommends\
    unzip zlib1g-dev diffstat patch apt-file
apt-file update


apt-get clean
rm -rf /var/lib/apt/lists/*

